# Leaflet.Maidenhead

An implementation of the [Maidenhead Locator System grid](https://en.wikipedia.org/wiki/Maidenhead_Locator_System) for [Leaflet](https://www.leafletjs.com)


Check out the [demo](https://ivansanchez.gitlab.io/leaflet.maidenhead/demo.html).


### Usage example

```html
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
<script src="https://unpkg.com/leaflet.maidenhead@1.0.0/src/maidenhead.js"></script>
<script type='text/javascript'>

L.maidenhead({ precision: 2 }).addTo(map);

</script>
```

### Available options

```javascript
L.maidenhead({
	// Precision of the grid, given as number of characters in the identifiers.
	// Must be 2, 4, 6 or 8. Defaults to 6.
	precision: 6,

	// A set of Polygon options, used to style each grid square.
	// The default is to enable fill but make it transparent, so that
	// the polygons can trigger mouse/pointer events.
	// (as per https://leafletjs.com/reference-1.5.0.html#polygon)
	polygonStyle: {
		color: "black",
		weight: 1.5,
		fill: true,
		fillColor: "transparent",
		fillOpacity: 0,
	},

	// Optional callback function for creating markers in the center of the seen
	// squares. Receives two parameter (the latlng of the center of a square,
	// and the current precision) and must return an instance of L.Layer.
	// The default is to create a L.Marker with a L.DivIcon showing
	// the code for the square.
	spawnMarker: function(latlng, precision) { return L.marker(latlng, { /* ... */ }) }
});
```

`L.Maidenhead` inherits from `L.FeatureGroup`, so keep in mind that it's possible to attach events, e.g.:

```
var grid = L.maidenhead({ /* etc */ });

grid.on('click', function(ev){
	console.log(ev.sourceTarget);
});
```

In order to not display the markers on the center of each grid square, set the `spawnMarker` option to false, e.g.:

```
var grid = L.maidenhead({ spawnMarker: false });
```

### Static methods

Want to calculate maidenhead grid stuff without a `L.Maidenhead` instance? These static methods will help:

```javascript
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
<script src="https://unpkg.com/leaflet.maidenhead@1.0.0/src/maidenhead.js"></script>
<script type='text/javascript'>

// Given latitude, longitude and precision, return a string with the corresponding
// Maidenhead locator.
console.log( L.Maidenhead.latLngToIndex(43.21, -3.98, 6) );

// Given a Maidenhead locator, return its bounding box (as a [lat, lng, lat, lng]
// or [y1, x1, y2, x2] array).
console.log( L.Maidenhead.indexToBBox("IO98by") );

</script>
```


## Legalese

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
